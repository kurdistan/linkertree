
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@iranluttes"></a>
   <a rel="me" href="https://kolektiva.social/@kurdistanluttes"></a>


|FluxWeb| `RSS <https://kurdistan.frama.io/linkertree/rss.xml>`_

.. _linkertree_kurdistanluttes:

=====================================================================
**Liens kurdistan**
=====================================================================


kurdistanluttes
==========================

- :ref:`Luttes au kurdistan <kurdistan_luttes:luttes_kurdistan>`

AIAK Isère
==============

- https://aiak.frama.io/aiak-info

CDKF (Le Conseil démocratique kurde en France)
==================================================

- https://cdkf.fr/
- https://www.instagram.com/codem_kurde/

Le Conseil démocratique kurde en France (CDK-F) est une structure qui
regroupe et fédère 26 associations de la diaspora kurde à travers toute la France.

Anciennement Fédération des Associations kurdes en France (FEYKA), le
CDK-F a pris son nom actuel en 2014, suite à une modification en profondeur
de ses statuts destinée à introduire un système de démocratie participative
dans l’ensemble de son organisation et de son fonctionnement.

Comme le prévoient ses statuts, le CDK-F a pour mission d’aider au
rapprochement des membres de la communauté kurde, de sauvegarder et
d’améliorer la langue de celle-ci, son identité et ses valeurs, de
défendre ses libertés, ses droits sociaux, culturels, économiques et
politiques, tout en respectant ceux des autres communautés et les
valeurs universelles, …

Depuis sa création en 1994, le CDKF œuvre pour faire connaître les Kurdes,
leur culture, leur langue, leur situation politique dans chacun des pays
qui se divisent le Kurdistan.

Principale organisation représentative des Kurdes en France, il mène des
actions au niveau national pour la reconnaissance des droits légitimes
du peuple kurde et pour dénoncer toutes les violations dont celui-ci
est victime.



Institut Kurde
================

- https://www.institutkurde.org/


KCK - Kurdistan Democratic Communities Union
===============================================

- https://kck-info.com/
- https://kck-info.com/ouridea/
- https://mastodon.social/@KCK_Kurdistan_

- :ref:`jjr:jjr_kck_2023_12_24`


Kurdistan au féminin
========================

- https://kurdistan-au-feminin.fr/


Rojava luttes
================

- https://rojava.frama.io/luttes/

serhildan
==============

- https://serhildan.org

ypj-info
==========

- https://ypj-info.org/


**RAAR (Réseau d'Actions contre l'Antisémitisme et tous les Racismes)**
==========================================================================

- https://raar.frama.io/linkertree/

Liens Grenoble
====================

- https://grenoble.frama.io/linkertree/
