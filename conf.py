# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Liens kurdistan"
html_title = project


author = f"kurdistanluttes"
html_logo = "images/logo_jina_amini_small.png"
html_favicon = "images/logo_jina_amini_small.png"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]
extensions += ["sphinx.ext.intersphinx"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "kurdistan_luttes": ("https://kurdistan.frama.io/luttes/", None),
    "rojava_luttes": ("https://rojava.frama.io/luttes/", None),
    "iran_luttes": ("https://iran.frama.io/luttes/", None),
    "jjr": ("http://jjr.frama.io/juivesetjuifsrevolutionnaires", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "http://kurdistan.frama.io/linkertree",
    "repo_url": "https://framagit.org/kurdistan/linkertree",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "green",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "table_classes": ["plain"],
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://aiak.frama.io/aiak-info",
            "internal": False,
            "title": "AIAK",
        },
        {
            "href": "https://cdkf.fr/",
            "internal": False,
            "title": "CDK-F",
        },
        {
            "href": "https://rojava.frama.io/luttes/",
            "internal": False,
            "title": "Rojava luttes",
        },
        {
            "href": "https://kurdistan-au-feminin.fr/",
            "internal": False,
            "title": "Kurdistan au féminin",
        },
        {
            "href": "https://kurdistan.frama.io/luttes/",
            "internal": False,
            "title": "Kurdistan luttes",
        },
    ],
    "heroes": {
        "index": "Liens kurdistanluttes",
    },
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True


copyright = f"2022-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"


rst_prolog = """
.. |JinaAmini| image:: /images/mahsa_jina_amini_avatar.png
.. |cdkf_fr| image:: /images/cdkf_fr_avatar.png
.. |place_grenette| image:: /images/place_grenette_avatar.png
.. |EmineKara| image:: /images/emine_kara_avatar.jpg
.. |AbdurrahmanKizil| image:: /images/abdurrahman_kizil_avatar.jpg
.. |MirPerwer| image:: /images/mir_perwer_avatar.png
.. |FidanDogan| image:: /images/fidan_dogan_avatar.png
.. |SakineCansiz| image:: /images/sakine_cansiz_avatar.png
.. |LeylaSoylemez| image:: /images/leyla_soylemez_avatar.jpg
.. |NagihanAkarsel| image:: /images/nagihan_akarsel_avatar.png
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
